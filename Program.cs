using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companiesNumber * companyRevenue * tax / 100;
        }

        public string GetCongratulation(int input)
        {
            string output = "Поздравляю c " + input + "-летием!";
            if (input % 2 == 0 && input >= 18)
                output = "Поздравляю с совершеннолетием!";
            else if (input % 2 != 0 && input > 12 && input < 18)
                output = "Поздравляю с переходом в старшую школу!";
            return output;
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            first = first.Replace('.', ',');
            second = second.Replace('.', ',');
            return double.Parse(first) * double.Parse(second);
        }

        public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            double perimeter = 0, square = 0, p = 0;
            switch ((int)figureType)
            {
                case 0:
                    perimeter = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                    if (dimensions.SecondSide == 0)
                        square = dimensions.FirstSide * dimensions.Height / 2;
                    else
                    {
                        p = perimeter / 2;
                        square = Math.Sqrt(p * (p - dimensions.FirstSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide));
                    }
                    break;
                case 1:
                    perimeter = (dimensions.FirstSide + dimensions.SecondSide) * 2;
                    square = dimensions.FirstSide * dimensions.SecondSide;
                    break;
                case 2:
                    if (dimensions.Radius == 0)
                        dimensions.Radius = dimensions.Diameter / 2;
                    perimeter = 2 * Math.PI * dimensions.Radius;
                    square = Math.PI * dimensions.Radius * dimensions.Radius;
                    break;
            }
            if ((int)parameterToCompute == 0)
                return Math.Round(square);
            return Math.Round(perimeter);
        }
    }
}
